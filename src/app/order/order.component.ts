import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from '../shared/models/cart';
import { CartItem } from '../shared/models/cartitem';
import { Order } from '../shared/models/order';
import { AuthService } from '../shared/services/auth.service';
import { OrderService } from '../shared/services/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit{

  order: Order[] = []
  purchases: Cart[] = []
  total: number[] = []
  
  constructor(public authService: AuthService, private orderService: OrderService, private router: Router) { }

  ngOnInit() {
    this.orderService.getOrder()
      .subscribe((res) => {
          this.order = res;

          for(var o of this.order){
            var t = 0
            for(var p of o.purchases){
              t += p.product.price * p.qty
            }
            this.total.push(t);
          }
          
          // console.log(res);
        }
      );
  }
}
