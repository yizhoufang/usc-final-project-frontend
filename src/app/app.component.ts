import { Component, OnInit } from '@angular/core';
import { AuthService } from './shared/services/auth.service';
import { OrderService } from './shared/services/order.service';
import { ProductService } from './shared/services/product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {

  // is_Admin = false;
  // addressTab: String = ""

  constructor(
    public authService: AuthService,
    public productService: ProductService,
    public orderService: OrderService
  ){ }

  // ngOnInit(): void {
  //   this.authService.isAdmin()
  //   .subscribe((res) => {
  //       this.is_Admin = res;
  //       console.log(res);
  //       return res;
  //     }
  //   );
  // }

  // CheckAdmin(): boolean {
  //   this.authService.isAdmin()
  //   .subscribe((res) => {
  //       this.is_Admin = res;
  //       console.log(res);
  //     }
  //   );
  //   return this.is_Admin;
  // }

  
  title = 'OnlineShopping';
}
