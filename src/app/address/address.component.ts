//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';
import { OrderService } from '../shared/services/order.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  response;

  constructor(public authService: AuthService, private orderService: OrderService,
    private router: Router) { }

  ngOnInit(): void {
    this.authService.checklogin()
      .subscribe((res) => {
        this.response = res;
        console.log(res);
        if(res.success) {
          // this.router.navigate(['/']);
        }
      });
  }

  addAddress(address: Address) {
    this.orderService.addAddress(address)
      .subscribe((res) => {
        this.response = res;
        if(res.success) {
          localStorage.setItem('isAdded','true')
          this.router.navigate(['/cart']);
        }
      });
  }
}
