import { NgModule } from "@angular/core";
import { Router, RouterModule, Routes } from "@angular/router";
import { AddressComponent } from "./address/address.component";
import { AppGuard } from "./app.guard";
import { CartComponent } from "./cart/cart.component";
import { LoginComponent } from "./login/login.component";
import { LogoutComponent } from "./logout/logout.component";
import { OrderComponent } from "./order/order.component";
import { ProductComponent } from "./product/product.component";
import { RegisterComponent } from "./register/register.component";

const routes: Routes = [
    {
      path: '',
      redirectTo: 'product',
      pathMatch: 'full',
    },
    {
      path:'product',
      component: ProductComponent
    },
    {
      path:'cart',
      component: CartComponent
    },
    {
      path: '',
      canActivate: [AppGuard],
      children: [
        {
          path: 'order',
          component: OrderComponent
        },
        {
          path: 'users/addAddress',
          component: AddressComponent
        },
        {
          path: 'logout',
          component: LogoutComponent
        }
      ]
    },
    {
      path: 'login',
      component: LoginComponent
    },
    {
      path: 'register',
      component: RegisterComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }