import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './shared/services/auth.service';

@Injectable()
export class AppGuard implements CanActivate {
    constructor (
        private router: Router,
        private authService: AuthService
    ) { }

    canActivate(
        next: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.authService.loggedIn
            .pipe((isLoggedIn) => {
                if(!isLoggedIn) {
                    this.router.navigate(['/login']);
                }
                return isLoggedIn;
            });
    }
}