import { Injectable } from '@angular/core';
import { AppConfig } from './app.config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private API_URL = AppConfig.API_URL;

  // loggedIn: Subject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, private router: Router) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.API_URL + "/product");
  }



}
