import { Injectable } from '@angular/core';
import { AppConfig } from './app.config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable, pipe, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Order } from '../models/order';
import { Address } from '../models/address';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private API_URL = AppConfig.API_URL;
  
  constructor(private http: HttpClient, private router: Router) { }

  addOrder(order: Order): Observable<any> {
    return this.http.post<any>(this.API_URL + "/order", order, {withCredentials: true});
  }

  getOrder(): Observable<Order[]> {
    return this.http.get<any>(this.API_URL + "/order", {withCredentials: true});
  }

  addAddress(address: Address): Observable<any> {
    return this.http.post<any>(this.API_URL + "/users/addAddress", address, {withCredentials: true});
  }
}
