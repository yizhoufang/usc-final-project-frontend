import { CartItem } from "./cartitem";

export class Order {
    purchase_date: Date;
    purchases: CartItem[];


    constructor(purchase_date: Date, purchases: CartItem[]) {
        this.purchase_date = purchase_date;
        this.purchases = purchases;
    }
}