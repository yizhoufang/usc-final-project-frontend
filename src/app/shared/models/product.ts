export class Product {
    id: number;
    name: string;
    price: number;
    image: URL;

    constructor(id: number, name: string, price: number, image: URL) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }
}