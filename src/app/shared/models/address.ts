export class Address {
    address: String
    city: String
    state: String
    zipcode: number

    constructor(address: String, city: String, state: String, zipcode: number) {
        this.address = address
        this.city = city
        this.state = state
        this.zipcode = zipcode
    }
}