import { CartItem } from "./cartitem";

export class Cart {
    cartItem: CartItem[];

    constructor(cartItem: CartItem[]) {
        this.cartItem = cartItem;
    }
}