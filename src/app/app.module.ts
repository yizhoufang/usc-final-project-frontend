import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing-module';
import { ProductComponent } from './product/product.component';
import { ProductService } from './shared/services/product.service';
import { CartComponent } from './cart/cart.component';
import { OrderService } from './shared/services/order.service';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthService } from './shared/services/auth.service';
import { AppGuard } from './app.guard';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { OrderComponent } from './order/order.component';
import { AddressComponent } from './address/address.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    CartComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    OrderComponent,
    AddressComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    ProductService,
    OrderService,
    AuthService,
    AppGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
