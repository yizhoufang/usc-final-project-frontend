//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { ProductService } from '../shared/services/product.service';
import { Router } from '@angular/router';
import { Product } from '../shared/models/product';
import { CartItem } from '../shared/models/cartitem';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product: Product[] = [];
  cart: CartItem[] = [];
  key = 0;

  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit() {
      this.productService.getProducts()
      .subscribe((res) => {
          this.product = res;
          // console.log(res);
        }
      );
  }

  addToCart(p: Product){
    var qty = prompt("Enter quantity:");
    var cartItem = new CartItem;
    cartItem.product = p;
    cartItem.qty = qty;
    // localStorage.setItem(this.key.toString(), JSON.stringify(cartItem))
    // console.log(JSON.parse(localStorage.getItem(this.key.toString())))
    // this.key++;
    // console.log(localStorage)
  

    this.cart.push(cartItem);
    // console.log(this.cart)
    localStorage.setItem('cart', JSON.stringify(this.cart))
    // var retrievedObject = localStorage.getItem('cart');
    // var a = JSON.parse(retrievedObject)
    // console.log(a[0].product)
    // console.log(a[0].qty)
    // console.log(a[0].product.name)
    // console.log(JSON.parse(retrievedObject))
  }
}
