//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  response;

  constructor(public authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    this.authService.checklogin()
      .subscribe((res) => {
        this.response = res;
        console.log(res);
        if(res.success) {
          this.router.navigate(['/']);
        }
      });
    
    // this.authService.isAdmin()
    //   .subscribe((res) => {
    //     console.log(res)
    //   })
  }

  login(user) {
    this.authService.login(user)
      .subscribe((res) => {
        this.response = res;
        if(res.success) {
          // this.router.navigate(['/']);
        }
      });

    
  }
}
