//@ts-nocheck
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartItem } from '../shared/models/cartitem';
import { Order } from '../shared/models/order';
import { Product } from '../shared/models/product';
import { AuthService } from '../shared/services/auth.service';
import { OrderService } from '../shared/services/order.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit{
  cart: CartItem[] = [];
  subtotal = 0;
  response;
  msg = "";
  // isAdded: boolean = false;

  constructor(public authService: AuthService, private orderService: OrderService, private router: Router) { }

  ngOnInit() {
    this.cart = JSON.parse(localStorage.getItem('cart') || '[]');
    // console.log(this.cart)
    for(var c of this.cart){
      var temp = c.product.price * c.qty
      this.subtotal += temp
    }
  }

  removeFromCart(id: number){
    var updatedCart: CartItem[] = []
    // var tempProduct: Product
    // var tempQty: number
    for(var c of this.cart){
      if(c.product.id == id){
        var temp = c.product.price * c.qty
        this.subtotal -= temp
      }else{
        // tempProduct = c.product
        // tempQty = c.qty
        var cartItem = new CartItem(c.product, c.qty);
        updatedCart.push(cartItem);
        // console.log(updatedCart)
      }
    }
    localStorage.setItem('cart', JSON.stringify(updatedCart))
    this.cart = updatedCart
    // console.log(localStorage)
  }

  clearCart(){
    // this.show = false;
    localStorage.clear();
    this.cart = [];
    this.subtotal = 0;
    // console.log(localStorage)
  }

  checkOut(){
    if(localStorage.getItem("cart") == null){
      this.msg = "No items in cart!"
    }else{
      this.authService.checklogin()
      .subscribe((res) => {
        this.response = res;
        console.log(res);
        if(!res.success) {
          this.router.navigate(['/login']);
        }else{
          if(localStorage.getItem('isAdded')=='true'){
            var order: Order = {
              purchase_date: new Date(),
              purchases: this.cart
            };
      
            this.orderService.addOrder(order).subscribe((res) => {
              this.cart = [];
              this.subtotal = 0;
              localStorage.clear();
            })
          }else{
            this.router.navigate(['users/addAddress'])
          }
          // this.router.navigate(['/order'])
        }
      })
    }
    
  }

}
