//@ts-nocheck
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CartComponent } from './cart.component';
import { OrderService } from '../shared/services/order.service';

describe('CartComponent', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;
  let service: OrderService

  beforeEach(async () => {
    //@ts-ignore
    service = new OrderService();
    var store = {};
    spyOn(localStorage, 'getItem').and.callFake( (key:string):String => {
     return store[key] || null;
    });
    spyOn(localStorage, 'removeItem').and.callFake((key:string):void =>  {
      delete store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake((key:string, value:string):string =>  {
      return store[key] = <string>value;
    });
    spyOn(localStorage, 'clear').and.callFake(() =>  {
        store = {};
    });

    await TestBed.configureTestingModule({
      declarations: [ CartComponent ],
      imports: [HttpClientTestingModule], 
      providers: [{provide: OrderService}]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add product to cart', () => {
    expect(localStorage.setItem('cart', 'product')).toBe('product');
    expect(localStorage.getItem('cart')).toBe('product');
  });

  it('should clear cart', () => {
    expect(localStorage.setItem('cart', 'product')).toBe('product');
    expect(localStorage.getItem('cart')).toBe('product');
    expect(localStorage.clear()).toBeUndefined();
    expect(localStorage.getItem('cart')).toBeNull();
  })
});
